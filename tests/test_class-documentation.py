import os

from mkdpdf.documentation.documentation import Documentation

class PyClass:
    def __init__(self):
        self.init = None

def test():

    # initialize
    d = Documentation()

    ##############################
    # SUBTEMPLATE_FUNCTIONS TEST #
    ##############################
    result = d.SUBTEMPLATE_FUNCTIONS(list())

    assert isinstance(result, str)

    #########################
    # SUBTEMPLATE_INIT TEST #
    #########################
    result = d.SUBTEMPLATE_INIT(PyClass())

    assert isinstance(result, str)

    ##################
    # TRANSPILE TEST #
    ##################
    result = d.transpile(dict(), "this is the header")
    assert isinstance(result, str)
    result = d.transpile(dict(), "this is the main part")
    assert isinstance(result, str)
    result = d.transpile(dict(), "this is the footer")
    assert isinstance(result, str)

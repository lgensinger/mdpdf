import os

from mkdpdf.document.document import Document

def test():

    # initialize
    d = Document()

    #################
    # ASSEMBLE TEST #
    #################
    d.assemble("main", "header", "footer")

    assert True

    ##################
    # CONSTRUCT TEST #
    ##################
    result = d.construct("header", "main", "footer")

    assert result is not None

    #################
    # GENERATE TEST #
    #################
    result = d.generate("main", "header", "footer", to_file=True)

    assert isinstance(result, str)
    assert os.path.exists(d.file_path)

    with open(d.file_path, "r") as f:
        assert f.read() == result

    #################
    # TEMPLATE TEST #
    #################
    result = d.template("header", "this is the header")
    assert isinstance(result, str)
    result = d.template("main", "this is the main part")
    assert isinstance(result, str)
    result = d.template("footer", "this is the footer")
    assert isinstance(result, str)

    ##################
    # TRANSPILE TEST #
    ##################
    result = d.template(dict(), "this is the header")
    assert isinstance(result, str)
    result = d.template(dict(), "this is the main part")
    assert isinstance(result, str)
    result = d.template(dict(), "this is the footer")
    assert isinstance(result, str)

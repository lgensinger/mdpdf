import os
import weasyprint

from mkdpdf import configuration
from mkdpdf.pdf.pdf import PDF

FILE_PATH = os.path.join(configuration.DIRECTORY_PATH_PACKAGE, "test")

def test():

    # initialize
    p = PDF(os.path.join(configuration.DIRECTORY_PATH_PACKAGE, "document", "templates", "pdf"))

    ##################
    # CONSTRUCT TEST #
    ##################
    result = p.construct("header", "main", "footer")

    assert isinstance(result, weasyprint.document.Document)

    ###############
    # RENDER TEST #
    ###############
    p.render(result, FILE_PATH)

    assert os.path.exists(FILE_PATH)

    ###############
    # TABLE TEST #
    ###############
    table = """
    | this | is |
    | :-- | :-- |
    | a | test |
    """
    result = p.table(table)

    assert isinstance(result, str)

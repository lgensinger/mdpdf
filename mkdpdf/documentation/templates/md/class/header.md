# CLASS_NAME

[_PACKAGE_NAME vPACKAGE_VERSION_](URL_RELEASE)

CLASS_NAME is a part of the **PACKAGE_NAME:** _PACKAGE_DESCRIPTION_

To get started with installation and setup, make sure to follow the guidance of the [README](URL_GIT). Below outlines all available methods inside the `CLASS_NAME` class in the PACKAGE_NAME package. When the default for a given parameter is listed as `configuration.XX` or `os.environ['XX']` this indicates that the value is populated from the environment via the package `configuration` or directly from the environmental variable.
